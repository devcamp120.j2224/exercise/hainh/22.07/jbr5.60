package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;

@Service
public class CustomerService {

    Customer cus1 = new Customer("Hải");
    Customer cus2 = new Customer("Nguyễn Hải");
    Customer cus3 = new Customer("Hoàng Hải");

    public ArrayList<Customer> getCustomerList(){

        ArrayList<Customer> allCus = new ArrayList<>();

        allCus.add(cus1);
        allCus.add(cus2);
        allCus.add(cus3);

        return allCus ;
    }

}
